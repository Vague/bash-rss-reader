# vague
# RSS feed readers suck. All of them (that I tried) are painful to use. You have to click 5 times to add a single url, you can't organize the output view like you want. You have to install... things... 
# Wecome to equally painful rss, the only reader that sucks the way I'm 80% happy about. It uses only bash coreutils and wget. It downloads items to individual files that you can use your platform's gui file browser to view in any order that pleases you. If something bothers you, change it. Whatever. 
# Thu Jun  6 08:16:12 PM EDT 2024




#urls=$(shuf urls.txt)
#urls="https://www.animenewsnetwork.com/all/rss.xml?ann-edition=us"
#urls="https://blog.openai.com/rss/" # doesnt work, broken site
#urls="https://engineering.grab.com/feed.xml" # used 'updated' as a word
#urls="http://githubengineering.com/atom.xml https://slack.engineering/feed"
urls=$(wget -O - https://raw.githubusercontent.com/tuan3w/awesome-tech-rss/main/README.md | grep ^htt.* | shuf)



get_feed_field(){
	# good for both atom and rss!
	# newline's any occurence of 'title', picks first one between > <
		# ran into an issue where an article actualled ued 'updated' so i added < to the pattern so it wouldnt trigger when it sees that 
		# element_published=$(echo $element | get_feed_field \<updated)
	field=$1
	cat | sed -e "s/$field/\n&/g" | head -n 2 | tail -n 1 | cut -d '>' -f 2 | cut -d'<' -f 1
}

process_feed(){
	url=$1
	page=$(timeout 5 wget -q --timeout=4  -O -  $url )
	# fix formatting
	page=$(echo $page | sed -e 's/<!\[CDATA\[//g' -e 's/]]//g' -e 's/&#8217;//g' -e 's/&#39;//g' -e 's/&#039;//g' -e 's/&quot;//g' -e 's/&#8220;//g' -e 's/&#8217;//g' -e 's/&#8221;//g' -e 's/&#8211;/-/g' -e 's/&#8212;/-/g' -e 's/&amp;/\&/g' -e 's/&#038;/\&/g' -e 's/&#062/>/g' -e 's/&lt;/</g' -e 's/&#x27;//g' -e 's/&gt;/>/g' ) 

	feed_title=$(echo $page | get_feed_field title | cut -c 1-50)
	
	[ -z "$feed_title" ] && feed_title="$url"
	
	echo "feed title: $feed_title | $url"

	echo $page | sed -e 's/<entry>/\n&/g' -e 's/<item>/\n&/g' | tail -n +2 | while read -r element ; do
		{
		#echo got $element
		element_title=$(echo $element | get_feed_field title)
		#echo ele title $element_title

		# get date
		# atom and rss have different date fields, atom has multiple date fields. 
		# pick your favorite, in order. if one doesnt exist then get the next type of date
		#echo get date
		element_pubdate=$(echo $element | get_feed_field pubDate)
		element_published=$(echo $element | get_feed_field \<published)
		element_updated=$(echo $element | get_feed_field \<updated)
		#time element_date=$(echo "$element_published | $element_pubdate | $element_updated" | sed -e 's/|/\n/g' | sed '/^[[:space:]]*$/d' | head -n 1 )
		#time element_date=$(printf "$element_published \n $element_pubdate \n $element_updated"  | sed '/^[[:space:]]*$/d' | head -n 1 )
		element_date=$(printf "$element_pubdate \n $element_published \n $element_updated"  | sed '/^[[:space:]]*$/d' | head -n 1 )

#		element_date=$(cat <<- EOF | sed '/^[[:space:]]*$/d' | head -n 1 
#			$element_pubdate
#			$element_published
#			$element_updated
#		EOF
#		)

		#echo got date $element_date


		echo " $feed_title | $element_title | $element_date "
		filename="$feed_title | $element_title"
		filename=$(echo $filename | sed -e 's_/_-_g') # change / to -
		echo $element > "$filename"
		
		# redate the file so we can use the filemanager to see elements in chronological order
		touch -d "$(date -d "$element_date")" "$filename"
		
		} &
	done
	wait
}


mkdir rss -p
cd rss || exit 1

for url in $urls; do
	process_feed $url &
done

wait 
